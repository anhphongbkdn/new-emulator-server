FROM node:16.15.1-alpine

WORKDIR /app

RUN npm install -g pm2

RUN apk update && apk add nano

COPY ["package.json", "package-lock.json*", "./"]

COPY . .

# Install node_modules
RUN npm install --production --silent

# Create a group and user
RUN addgroup -S appgroup && adduser -S appuser -G appgroup

# Tell docker that all future commands should run as the appuser user
USER appuser

# Development
#CMD ["npm", "run", "dev"]

# Production
CMD ["pm2-runtime", "ecosystem.config.js", "--env", "production"]

