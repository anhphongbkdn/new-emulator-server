const Images = require('../models/api/Image');

class TextController {
    static async get(req, res) {
        try {
            var images = await Images.find({ uid: req.uid });
            if (!images) {
                return res.json({ success: false, message: 'Not found!' });
            }

            return res.json({ success: true, data: images });
        } catch (error) {
            return res.json({ success: false, message: error });
        }
    }

    static async add(req, res) {
        try {
            const imageInfo = await req.body.info;
            //console.log(imageInfo);
            var json = JSON.parse(imageInfo);
            // console.log(json);

            var images = await Images.findOne({ uid: req.uid, name: json['Name'] });
            if (images) {
                return res.json({ success: false, message: 'Image exited!' });
            }

            new Images({
                uid: req.uid,
                name: json["Name"],
                group: json["Group"],
                base64: json["Base64"],
                // command: json['Command'],
                createTime: json["CreateTime"]
            }).save();

            return res.json({ success: true });
        } catch (error) {
            return res.json({ success: false, message: error });
        }
    }

    static async remove(req, res) {
        try {
            const name = req.body.name;
            // console.log(name);
            var json = JSON.parse(name);
            // console.log(json);
            var images = await Images.findOneAndRemove({ uid: req.uid, name: json });
            if (images) {
                return res.json({ success: true });
            }

            return res.json({ success: false, message: 'Not found!' });

        } catch (error) {
            return res.json({ success: false, message: 'Not found!' });
        }
    }
}

module.exports = TextController;