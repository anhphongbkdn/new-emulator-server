const Script = require('../models/api/Script');

class TextController {
    static async get(req, res) {
        try {
            const script = await Script.find({ uid: req.uid });
            if (!script) {
                return res.json({ success: false, message: 'Not found' });
            }
            return res.json({ success: true, data: script });
        } catch (error) {
            return res.json({ success: false, message: error });
        }
    }

    static async add(req, res) {
        try {
            const info = req.body.info;
            if (!info) {
                return res.json({ success: false, message: 'Not found!' });
            }

            var json = JSON.parse(info);
            var script = await Script.findOne({
                uid: req.uid,
                type: json['Type'],
                packageName: json['PackageName']
            });

            if (script) {
                return res.json({ success: false, message: 'Script exited!' });
            }

            new Script({
                uid: req.uid,
                type: json['Type'],
                packageName: json['PackageName'],
                content: json['Content']
            }).save((err, data) => {
                if (err) {
                    console.log(err);
                }

                console.log(data);

            });

            return res.json({ success: true, message: 'Script added!' });
        } catch (error) {
            return res.json({ success: false, message: error });
        }
    }

    static async remove(req, res) {
        try {
            const info = req.body.info;
            if (!info) {
                return res.json({ success: false, message: 'Not found!' });
            }

            var json = JSON.parse(info);
            var script = await Script.findOneAndRemove({
                uid: req.uid,
                type: json['Type'],
                packageName: json['PackageName']
            });

            if (script) {
                return res.json({ success: false, message: script });
            }

        } catch (error) {
            return res.json({ success: false, message: error });
        }
    }
}

module.exports = TextController;