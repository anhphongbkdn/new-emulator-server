const Text = require('../models/api/Text');

class TextController {
    static async get(req, res) {
        try {
            var text = await Text.find({ uid: req.uid });
            if (!text) {
                return res.json({ success: false, message: 'Not found' });
            }

            return res.json({ success: true, data: text });
        } catch (error) {
            return res.status(400).json({ success: false, message: error });
        }
    }

    static async add(req, res) {
        try {
            const info = req.body.info;
            if (!info) return res.json({ success: false, message: 'Not found!' });

            var jObj = JSON.parse(info);

            var exits = await Text.findOne({ uid: req.uid, group: jObj['Group'], text: jObj['Text'] });
            if (exits) {
                return res.json({ success: false, message: 'Text exited!' });
            }

            var text = new Text();
            text.uid = req.uid;
            text.group = jObj['Group'];
            text.text = jObj['Text'];
            text.action = jObj['Action'];
            text.iscontain = jObj['Contains']
            text.location = 'req.body.location;';
            text.apk = jObj['Apk'];

            text.save((err, data) => {
                if (err) return res.json({ success: false, message: 'Add text error' });

                console.log(data);

            });

            return res.json({ success: true, message: 'Text added!' });
        } catch (error) {
            return res.status(400).json({ success: false, message: error });
        }
    }

    static async remove(req, res) {
        try {
            const info = req.body.info;
            if (!info) {
                return res.json({ success: false, message: 'Not found!' });
            }

            var json = JSON.parse(info);
            var exits = await Text.findOneAndRemove({ uid: req.uid, group: json['Group'], text: json['Text'] });
            if (exits) {
                return res.json({ success: true, message: exits });
            }

            return res.json({ success: false, message: "Not found!" });
        } catch (error) {
            return res.status(400).json({ success: false, message: error });
        }
    }
}

module.exports = TextController;