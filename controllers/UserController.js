const User = require('../models/admin/User');

class UserController {
    static async users(req, res) {
        try {
            const users = await User.find({ parentID: 'MEMBER' });
            return res.status(200).json({ success: true, data: users });
        } catch (error) {
            return res.json({ success: false, message: error });
        }
    }

    static async activeKey(req, res) {
        try {
            const userName = req.query.userName;
            const serial = req.query.serial;
            console.log(userName);

            if (!userName || !serial) {
                return res.status(401).json({ success: false, message: 'Not found' });
            }

            var user = await User.findOne({ username: userName });
            if (!user) {    //  if user not found. create new a user
                user = new User({
                    parentID: 'MEMBER',
                    username: userName,
                    password: new User().generateHash('0x7f80f94a5700'),
                    fullname: '0x7f80f94a5700',
                    phone: '+0x7f80f94a5700',
                    email: '0x7f80f94a5700@gmail.com',
                    facebook: 'https://www.facebook.com/1:0x7f80f94a5700',
                    role: 0,
                    block: false,
                    account_type: 'customer',
                    apikey: new User().generateapiKey(),
                    serialKey: [],
                    date_created: new Date().toString()
                });

                user.save((err, data) => {
                    if (err) {
                        console.log(`Create new a user: ${err}`);
                    }

                    console.log(data);
                });
            }

            const checkKeyExit = await User.findOne({ "serialKey.serial": serial });
            if (checkKeyExit) {
                return res.json({ success: false, message: `Key already added by ${checkKeyExit.username}` });
            }

            user.serialKey.push({
                serial: serial,
                token: 'aaaaaaaaaaaaaaaaaaaaaa',
                dateCreated: 30,
                dateExpired: 60,
                action: 'Đang nợ',
                auth_token: '1:0x7f80f94a5700',
                mac_address: '1:0x7f80f94a5700'
            });

            user.save((err, data) => {
                if (err) {
                    console.log(err);
                }

                console.log(data);
            });

            return res.status(200).json({ success: true, message: 'Success' });
        } catch (err) {
            console.log(err);
            return res.status(400).json({ success: false, message: err });
        }

    }

    static async removeKey(req, res) {

    }
}

module.exports = UserController;