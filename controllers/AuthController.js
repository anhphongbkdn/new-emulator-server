const User = require('../models/admin/User');

class AuthController {
    static async checkKey(req, res) {
        try {
            if (!req.query.serial) {
                return res.status(401).json({ success: false, message: 'Key not found' });
            }

            const serial = req.query.serial;
            // console.log(serial);
            const user = await User.findOne({ "serialKey.serial": serial });
            if (!user) {
                return res.status(404).json({ success: false, message: 'Key not found' });
            }

            res.setHeader('uid', user.apikey);
            res.setHeader('userName', user.username);
            
            return res.status(200).json({ success: true, message: 'Key actived!' });

        } catch (error) {
            return res.status(400).json({ success: false, message: error });
        }
    }
}

module.exports = AuthController;