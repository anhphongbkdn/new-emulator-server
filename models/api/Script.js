const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var script = new Schema({
    uid: { type: String, require: true },
    type: { type: String, require: true },
    packageName: { type: String, require: true },
    content: { type: String, require: true },
});

module.exports = mongoose.model('script', script);