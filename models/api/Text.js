const mongoose = require('mongoose');

var Schema = mongoose.Schema;

var texts = new Schema({
    uid: { type: String, require: true },
    group: { type: String, require: true },
    text: { type: String, require: true },
    action: String,
    iscontain: String,
    location: String,
    apk: String

});

module.exports = mongoose.model('text', texts);