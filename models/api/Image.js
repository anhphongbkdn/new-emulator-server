const mongoose = require('mongoose');

const Schema = mongoose.Schema;

var image = new Schema({
    uid: { type: String, require: true },
    name: { type: String, require: true },
    group: { type: String, require: true },
    base64: { type: String, require: true },
    command: { type: String},
    createTime: { type: String }
});

module.exports = mongoose.model('image', image);