const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const crypto = require('crypto');

var userSchema = mongoose.Schema({
    groupUID: String,
    parentID: { type: String, require: true, default: 'customer' },
    username: { type: String, require: true, unique: true },
    password: { type: String, require: true },
    fullname: { type: String, default: 'adzUser' },
    phone: { type: String, default: '' },
    email: { type: String, default: 'adz@gmail.com' },
    facebook: { type: String, require: true },
    role: { type: Number, default: 1 },
    block: { type: Boolean, default: false },
    account_type: { type: String, default: 'user' },
    apikey: { type: String, require: true, unique: true },
    serialKey:
        [{
            serial: { type: String, require: true, unique: true },
            token: { type: String, require: true },
            dateCreated: { type: String, require: true },
            dateExpired: { type: String, require: true },
            action: {
                type: String,
                enum: ["Dùng thử", "Đang nợ", "Đã thanh toán"],
                default: "Dùng thử"
            },
            auth_token: { type: String },
            mac_address: { type: String }
        }],
    date_created: { type: Date, default: Date.now() }
});

userSchema.methods.generateHash = (password) => {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8));
};

userSchema.methods.generateapiKey = () => {
    return crypto.randomBytes(16).toString('hex');
};

userSchema.methods.validPassword = (password, hash) => {
    return bcrypt.compareSync(password, hash);
};

module.exports = mongoose.model('user', userSchema);
