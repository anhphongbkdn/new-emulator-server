const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var auth = new Schema({
    uid: { type: String, require: true },
    serial: { type: String, require: true, unique: true }
});

module.exports = mongoose.model('authention', auth);