const User = require('../models/admin/User');
const mongoose = require('mongoose');
const dbHost = process.env.DB_HOST || 'localhost';
const dbPort = process.env.DB_PORT || 27017;
const dbName = process.env.DB_NAME || 'database';
const dbUser = process.env.DB_USER;
const dbUserPassword = process.env.DB_USER_PASSWORD;
const mongoUrl = `mongodb://${dbUser}:${dbUserPassword}@${dbHost}:${dbPort}/${dbName}?authSource=admin`;

const connectWithRetry = () => {
    return mongoose.connect(mongoUrl, err => {
        if (err) {
            console.log('Failed to connect to mongo on startup - retrying in 5s...');
            setTimeout(connectWithRetry, 5000);
        }
    });
}

connectWithRetry();

mongoose.connection.on('error', err => {
    console.log(`MongoDB connect error: ${err}`);
});

mongoose.connection.on('connected', () => {
    console.log('MongoDB is connected');
});

createNewRootUser = async () => {
    // try {
    //     // User.collection.getIndexes();
    //     const indexName = 'serialKey.serial_1';
    //     User.collection.dropIndex(indexName, (err, result) => {
    //         if(err) {
    //             console.log(err);
    //         }

    //         console.log(result);
    //     });
    // } catch(error) {
    //     console.log(error);
    // }

    const user = await User.findOne({ parentID: 'BOSS', account_type: 'root' }).exec();
    if (!user) {
        console.log('Creating new root user...');
        const root = new User({
            parentID: 'BOSS',
            username: 'anhphongbkdn',
            password: new User().generateHash('Phongbk!'),
            fullname: 'Hoang Nguyen Phong',
            phone: '+84397713246',
            email: 'anhphongbkdn@gmail.com',
            facebook: 'https://www.facebook.com/anhphongbkdn',
            role: 0,
            block: false,
            account_type: 'root',
            apikey: new User().generateapiKey(),
            serialKey: [],
            date_created: new Date().toString()
        });

        root.save((err, data) => {
            if (err) {
                console.log(`Create root user: ${err}`);
            }

            console.log(data);
        });
    } else {
        console.log(user);
    }
}

createNewRootUser();


// test = async () =>{
//     console.log('not found');
//     var user = new User();
//     console.log(user.generateHash('Phongbk!'));
//     console.log(user.generateapiKey());
//     console.log(user.validPassword('Phongbk!'))
// }

// test();
