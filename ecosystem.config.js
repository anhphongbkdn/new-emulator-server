/**
 * @author: Hoang Nguyen Phong
 * @last modified by: Hoang Nguyen Phong
 * @last modified time: 05/11/2022
 * @license by adz
 * @copyright @ 2022 Hoang Nguyen Phong. All rights reserved
 */
 module.exports = {
    apps: [{
      name: 'new-emulator-server',
      script: 'npm',
      args: 'start',
      time: true,
      exec_mode: 'fork', // need explicitly declare mode otherwise it will fallback to cluster mode and cause infinite reload
      instances: 1,
      autorestart: true,
      watch: false,
      max_memory_restart: '4G',
      env: {
        NODE_ENV: 'development'
      },
      env_production: {
        NODE_ENV: 'production'
      }
    }]
  }
  