const express = require('express');
const router = express.Router();
const auth = require('./auth');

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Express' });
});

const textController = require('../controllers/TextController');
const scriptController = require('../controllers/ScriptController');
const imageController = require('../controllers/ImagesController');


const authController = require('../controllers/AuthController');
router.get('/api/checkDevice', authController.checkKey);  // check serial or computerId


const userController = require('../controllers/UserController');
router.get('/api/users', userController.users);
router.get('/api/activeKey', userController.activeKey); //  add key for user
router.get('/api/deleteKey', userController.removeKey); //  delete key of user


router.route('/api/texts')
  .all(auth.customer)
  .get(textController.get)
  .post(textController.add)
  .patch(textController.remove);


router.route('/api/scripts')
  .all(auth.customer)
  .get(scriptController.get)
  .post(scriptController.add)
  .patch(scriptController.remove);


router.route('/api/images')
  .all(auth.customer)
  .get(imageController.get)
  .post(imageController.add)
  .patch(imageController.remove);


module.exports = router;
