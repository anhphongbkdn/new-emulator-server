const jwt = require('jsonwebtoken');
const User = require('../models/admin/User');


class auth {
    static async root(req, res, next) {
        next();
    }

    static async admin(req, res, next) {
        next();
    }

    static async customer(req, res, next) {
        const uid = req.headers['uid'];
        if (!uid) {
            return res.status(401).json({ success: false, message: 'Not found!' });
        }

        const user = await User.findOne({ apikey: uid });
        if (!user) {
            return res.status(404).json({ success: false, message: 'Not found!' });
        }

        console.log(user.username);

        req.userName = user.username;
        req.uid = uid;
        next();
    }
}

module.exports = auth;